# websphere8-centos7

Praticando container docker instalando o WebSphere 8 com base centos7 (https://hub.docker.com/_/centos). Projeto com base no github: https://github.com/WASdev/ci.docker.websphere-traditional/tree/master/docker-build/8.5.5.17. 

Download do instalador: https://github.com/WASdev/ci.docker.websphere-traditional/blob/master/docker-build/download-iim.md

## Build

```
docker build -t websphere8-centos7:v1.0.0 . --build-arg IBMID={ID_IBM} --build-arg IBMID_PWD={SENHA_ID_IBM}
```

## Run

```
docker run --name test -h test -v volume-websphere8-centos7:/work/ -p 9043:9043 -p 9443:9443 -d websphere8-centos7:v1.0.0
```

Endereco do console administrativo (user wsadmin): https://localhost:9043/ibm/console/login.do?action=secure

## Exec

```
docker exec -it test cat /tmp/PASSWORD
```

## Logs

```
docker logs -f --tail=all test
```